Project for creating map history of tribalwars worlds. It has two parts- webdriver that needs to run in regular periods and script that processes  raw screenshots that webdriver creates. In the end ffmpeg compiles processed screenshots into mp4.

Instructions:
1. webdriving.py - this script uses chromedriver.exe to run www.plemionamapa.pl and export screenshot of present state of the chosen world
2. prepare.py - used to turn raw images into prepared for video creation - adding date, adequate cropping, k nearest neighbours coloring. 
3. datesToNumbers.py - this script runs only on prepared folder and is used to neatly name screenshots for mp4 export using ffmpeg
3. makeMp4.bat - using ffmpeg creates mp4 from prepared folder - needs numbers in names of source files

Example final video:
https://www.youtube.com/watch?v=yPOS2OJaau0
