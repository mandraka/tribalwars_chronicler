import os, time
from PIL import Image, ImageDraw, ImageFont
from sklearn.neighbors import KNeighborsClassifier
from multiprocessing import Pool
import tqdm


#Good practice is to separate batches of photos into archives - for example: separate photos from month into two batches named accordingly with dates. Then use this script to make archives of prepared screenshots. Saves time - no need to reprocess everytime.

k = 3 # k for kNN

def assert_directories():
    print("Asserting presence of directory workspace\prepared")
    if not os.path.exists('workspace\prepared'):
        print("Creating directory workspace\prepared")
        os.makedirs('workspace\prepared')

def clear_workspace():
    print("Cleaning directory workspace\prepared")
    directory = r'workspace\prepared'
    for the_file in os.listdir(directory):
        file_path = os.path.join(directory, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            print(e)

def crop_image(filename):
    directory = r'workspace'
    if (filename.endswith(".jpg") or filename.endswith(".png")) and filename.startswith("20"):
            im = Image.open(os.path.join(directory, filename))
            width, height = im.size
            left = 1300
            top = 1100
            right = 3101
            bottom = 2921
            im1 = im.crop((left, top, right, bottom))
            im1.save(os.path.join('workspace\prepared', filename))

def crop_images():# cropping
    print("Cropping")
    directory = r'workspace'
    pool = Pool()
    length=len(os.listdir(directory))
    for _ in tqdm.tqdm(pool.imap_unordered(crop_image, os.listdir(directory)), total=length):
        pass

def fill_gap_image(filename, last):
    directory = r'workspace\prepared'
    if filename.endswith(".jpg") or filename.endswith(".png"):
            # Opens a image in RGB mode
            im = Image.open(os.path.join(directory, filename))
            if last != None:
                lastHour = last[11:13]
                thisHour = filename[11:13]
                gap = int(thisHour)-int(lastHour)
                if gap < 0:
                    gap += 24
                for x in range(1, gap):
                    last = last[0:11]+str(int(last[11:13])+1).zfill(2)+".00.png"
                    im.save(os.path.join(directory, last))
            last = filename
    return last

def fill_gap_images(): # multiplying images if needed to get an image per every hour
    print("Multiplicating")
    directory = r'workspace\prepared'
    length=len(os.listdir(directory))
    last = None
    for filename in tqdm.tqdm(os.listdir(directory), total=length):
        last = fill_gap_image(filename, last)

def draw_background(filename):
    directory = r'workspace\prepared'
    if filename.endswith(".jpg") or filename.endswith(".png"):
            knn_X = [] # position
            knn_y = [] # faction
            color_tuples_array = [] # color representations of factions
            neigh = KNeighborsClassifier(n_neighbors = k)
            im = Image.open(os.path.join(directory, filename))
            rgb_im = im.convert('RGB')
            width, height = im.size

            # take every fourth pixel in both dimensions - returns center of every point on map - 3x3 vilage + 1 pixel of gap
            #print("reading data from map")
            for y in range(1, int(height/4)):
                for x in range (1, int(width/4)):
                    r, g, b = rgb_im.getpixel(((x * 4) - 2, (y * 4) - 2))

                    if r != 67 and g != 98 and b != 19: # is not background green
                        knn_X.append([x,y])

                        value = None
                        iterator = 0
                        for tuple in color_tuples_array:
                            if r == tuple[0] and g == tuple[1] and b == tuple[2]:
                                value = iterator
                            iterator += 1
                        if value == None:
                            color_tuples_array.append((r,g,b))
                            value = iterator + 1
                        knn_y.append(value)

            #print("fitting arrays to model")
            neigh.fit(knn_X, knn_y)
            pixels = im.load()
            coords_array = []
            for y in range(1, int(height/4)):
                for x in range (1, int(width/4)):
                    r, g, b, a = pixels[(x * 4) - 2, (y * 4) - 2]
                    if r == 67 and g == 98 and b == 19: # is background green
                        coords_array.append([x,y])
            #print("getting predictions array from collected data")
            predictions = neigh.predict(coords_array)
            #print("coloring map according to predictions")
            iterator = 0
            for coord in coords_array:
                color_tuple = color_tuples_array[int(predictions[iterator])]
                r, g, b, a = pixels[((coord[0] * 4) - 2, (coord[1] * 4) - 2)]
                for j in range(1,4):
                            for i in range(1,4):
                                pixels[((coord[0] * 4) - i, (coord[1] * 4) - j)] = int((color_tuple[0]+r)/2), int((color_tuple[1]+g)/2), int((color_tuple[2]+b)/2)
                iterator += 1
            im.save(os.path.join(directory, filename))

def draw_backgrounds():
    # political map background drawing using kNearestNeighbour algorithm from sklearn library
    # it will firstly assign villages to X y where X is a vector of position (500,521 e.g.) and y as value that is its color
    # y values will be assigned to certain colors in tuples
    # reading villages gets color of pixel in center of each 3x3 square that map is divided to. If it is non neutral(non greenish) then point is added to train sets of KNN
    # as reading is done next step is to color every 3x3 field according to KNN prediction.

    print("Political background drawing")
    directory = r'workspace\prepared'
    pool = Pool(processes=4)
    length=len(os.listdir(directory))
    for _ in tqdm.tqdm(pool.imap_unordered(draw_background, os.listdir(directory)), total=length):
        pass

def write_datetime_to_image(filename):
    directory = r'workspace\prepared'
    fnt = ImageFont.truetype('Arial.ttf', 96)
    if filename.endswith(".jpg") or filename.endswith(".png"):
        #print(os.path.join(directory, filename))
        im = Image.open(os.path.join(directory, filename))
        time = ImageDraw.Draw(im)
        time.text((0,20), filename[5:-4],font=fnt, fill=(255,255,255))
        im.save(os.path.join(directory, filename))

def write_datetime_to_images(): # writing datetime in bottom right
    print("DateTime writing")
    directory = r'workspace\prepared'
    length=len(os.listdir(directory))
    for filename in tqdm.tqdm(os.listdir(directory), total=length):
        write_datetime_to_image(filename)

def append_legend(filename, last_legend):
    directory = r'workspace'
    directory_prepared = r'workspace\prepared'
    if filename.endswith(".jpg") or filename.endswith(".png"):
        if os.path.isfile(os.path.join(directory, filename)):
            im_raw = Image.open(os.path.join(directory, filename))
            width, height = im_raw.size
            left = 0
            top = 0
            right = 149
            bottom = 1821
            legend = im_raw.crop((left, top, right, bottom))
            last_legend = legend
        elif last_legend == None: # unable to execute , no way to get image
            return last_legend
        # last_legend is either new legend or old passed from other calls
        im_edited= Image.open(os.path.join(directory_prepared, filename))
        images = im_edited, last_legend
        widths, heights = zip(*(i.size for i in images))
        total_width = sum(widths)
        max_height = max(heights)
        new_im = Image.new('RGB', (total_width, max_height))
        x_offset = 0
        for im in images:
          new_im.paste(im, (x_offset,0))
          x_offset += im.size[0]
        new_im.save(os.path.join(directory_prepared, filename))
        return last_legend

def append_legends(): # appending legend to images on left
    print("Legend extraction and appending")
    directory_prepared = r'workspace\prepared'
    length=len(os.listdir(directory_prepared))
    last_legend = None
    for filename in tqdm.tqdm(os.listdir(directory_prepared), total=length):
        last_legend = append_legend(filename, last_legend)


if __name__ == '__main__':
    assert_directories()
    clear_workspace()
    crop_images()
    fill_gap_images()
    draw_backgrounds()
    write_datetime_to_images()
    append_legends()
