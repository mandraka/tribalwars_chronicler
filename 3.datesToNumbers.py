import os
import tqdm


def rename_to_iterator(filename, iterator): # renames image
    directory = r'workspace\prepared'
    if filename.endswith(".jpg") or filename.endswith(".png"):
        os.rename(os.path.join(directory, filename), os.path.join(directory, str(iterator)) + ".png")
        iterator += 1
        return iterator

def process_names(): # iterates through all images and for each sends above function
    print("Filename to iterator")
    directory = r'workspace\prepared'
    iterator = 1
    length=len(os.listdir(directory))
    for filename in tqdm.tqdm(os.listdir(directory), total=length):
        iterator = rename_to_iterator(filename, iterator)

     
if __name__ == '__main__':
    process_names()