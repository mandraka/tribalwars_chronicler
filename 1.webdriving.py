import time
import datetime
import os
import urllib.request as urllib
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains


# open site using chromedriver path provided
driver = webdriver.Chrome(r'C:\Users\konta\Desktop\chromedriver_win32\chromedriver.exe')

# generate link to satisfying plemionamapa configuration
driver.get('http://pl146.plemionamapa.pl/5dcef99abf188')

# close welcome message
driver.execute_script("display.panel('messages',0,1);")

# open menu
driver.execute_script("worldMap.options();")
driver.execute_script("worldMap.iconsize=3;")
driver.execute_script("worldMap.legend=1;")
time.sleep(0.4)

# click button
python_button = driver.find_elements_by_xpath("//input[@value='Go']")
python_button[0].click()
time.sleep(4)

# get the image source
img = driver.find_element_by_xpath('//img[@width=4201]')
src = img.get_attribute('src')

# download the image
now = datetime.datetime.now()
formattedNowString = str(now.year)+'.'+str(now.month).zfill(2)+'.'+str(now.day).zfill(2)+'-'+str(now.hour).zfill(2)+'.00.png'
urllib.urlretrieve(src, formattedNowString)
print(now)

driver.quit()